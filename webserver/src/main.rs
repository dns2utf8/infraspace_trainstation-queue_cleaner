#![allow(unused)]

use actix::{
    utils::TimerFunc, Actor, Addr, AsyncContext, ContextFutureSpawner, Handler, Message,
    StreamHandler,
};
use actix_web::{web, App, Error, HttpRequest, HttpResponse, HttpServer, Responder};
use actix_web_actors::ws;
use log::{debug, error, info, warn};
use serde::{Deserialize, Serialize};
use std::{
    sync::Arc,
    time::{Duration, Instant},
};

mod templates;
use templates::Templates;
mod messages;
use messages::{ClientCommand, ClientRequest, SubscribeMyWs, UnsubscribeMyWs};
mod stats;
use stats::Stats;

const MAX_FRAME_SIZE: usize = 4 * 1024 * 1024; //16_384; // 16KiB
const PING_PONG_DATA: [u8; 16] = [
    42u8, 23, 34, 63, 182, 202, 40, 13, 248, 73, 74, 92, 153, 205, 149, 137,
];
// keep the interval close to the TCP timeout
const KEEP_ALIVE_INTERVAL: Duration = Duration::from_secs(25);

#[actix_web::main] // or #[tokio::main]
async fn main() -> std::io::Result<()> {
    simple_logger::SimpleLogger::new()
        .with_level(log::LevelFilter::Info)
        .init()
        .unwrap();
    messages::debug_client_commands();

    let lobby = web::Data::new(Lobby::new());
    let stats = web::Data::new(Stats::new());

    let bind_addr = (std::net::Ipv6Addr::from([0u16, 0, 0, 0, 0, 0, 0, 1]), 46372);
    info!("binding to: {bind_addr:?}");

    HttpServer::new(move || {
        let lobby: web::Data<Addr<Lobby>> = lobby.clone();
        let stats: web::Data<Stats> = stats.clone();
        App::new()
            .app_data(templates::new())
            .app_data(lobby)
            .app_data(stats)
            .route("/", web::get().to(index))
            .route("/metrics", web::get().to(metrics))
            .route("/ws/", web::get().to(start_ws))
            .service(
                actix_files::Files::new("/static", "./static/")
                    .use_last_modified(true)
                    .use_etag(true)
                    .show_files_listing(),
            )
    })
    .bind(bind_addr)?
    .run()
    .await
}

async fn index(tmpl: web::Data<Templates>, stats: web::Data<Stats>) -> impl Responder {
    stats.index_inc();
    let user = UserContext;
    let mut tmpl = tmpl.get_render_context(&user);
    tmpl.render_page("index.html")
}

async fn metrics(tmpl: web::Data<Templates>, stats: web::Data<Stats>) -> impl Responder {
    use prometheus::Encoder;

    stats.metrics_inc();

    let encoder = prometheus::TextEncoder::new();
    let mut buffer = vec![];
    let metric_families = prometheus::gather();
    encoder.encode(&metric_families, &mut buffer).unwrap();

    HttpResponse::Ok().content_type("text/plain").body(buffer)
}

async fn start_ws(
    req: HttpRequest,
    stream: web::Payload,
    lobby: web::Data<Addr<Lobby>>,
    stats: web::Data<Stats>,
) -> Result<HttpResponse, Error> {
    let lobby: Addr<Lobby> = lobby.get_ref().clone();
    //let resp = ws::start(MyWs::new(lobby), &req, stream);
    let resp = ws::WsResponseBuilder::new(MyWs::new(lobby, stats.clone()), &req, stream)
        //.codec(Codec::new())
        //.protocols(&["A", "B"])
        .frame_size(MAX_FRAME_SIZE)
        .start();
    //debug!("{:?}", resp);
    if resp.is_ok() {
        stats.ws_ok_inc();
    } else {
        stats.ws_err_inc();
    }
    resp
}

/// Holds a Logged in User
pub struct UserContext;

/// Define HTTP actor
pub struct MyWs {
    lobby: Addr<Lobby>,
    stats: Arc<Stats>,
    next_file_name: Option<String>,
    upload_buffer: Vec<u8>,
    ping_start: Instant,
}
impl MyWs {
    fn new(lobby: Addr<Lobby>, stats: web::Data<Stats>) -> Self {
        Self {
            lobby,
            stats: stats.into_inner(),
            next_file_name: None,
            upload_buffer: vec![],
            ping_start: Instant::now(),
        }
    }

    fn ping_client(&mut self, context: &mut ws::WebsocketContext<Self>) {
        context.ping(&PING_PONG_DATA);
        self.ping_start = Instant::now();
        TimerFunc::new(KEEP_ALIVE_INTERVAL, Self::ping_client).spawn(context);
    }

    fn handle_upload(&mut self, ctx: &mut ws::WebsocketContext<Self>) {
        let name = self.next_file_name.take();
        let mut logger = common::Logger::default();

        let upload_buffer = std::mem::replace(&mut self.upload_buffer, Vec::new());

        match handle_zipped_upload(upload_buffer, &mut logger, ctx) {
            Ok(recompressed) => {
                ctx.binary(recompressed);
                self.stats.processing_ok_inc();
            }
            Err(err) => {
                ws_send_to_client(
                    ClientCommand::InvalidInput(format!("Binary Upload failed {err:?}")),
                    ctx,
                );
                self.stats.processing_err_inc();
            }
        }
        self.garbage_collect_buffer();
        ws_send_to_client(
            ClientCommand::BinaryProcessingLog {
                name,
                log: logger.finish(),
            },
            ctx,
        );
    }

    fn garbage_collect_buffer(&mut self) {
        // GC the buffer it is probably not going to be used in this session again
        self.upload_buffer = Vec::with_capacity(0);
    }
}

impl Actor for MyWs {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        debug!("MyWs started");
        self.lobby.do_send(SubscribeMyWs(ctx.address()));
        self.ping_client(ctx);
    }

    fn stopped(&mut self, ctx: &mut Self::Context) {
        debug!("MyWs stopped");
        self.lobby.do_send(UnsubscribeMyWs(ctx.address()));
        // this should fix the memory leak
        self.garbage_collect_buffer();
    }
}

/// Handler for ws::Message message
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for MyWs {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        match msg {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Pong(msg)) => {
                if *msg != PING_PONG_DATA {
                    error!("MyWs::StreamHandler invalid Pong data: {msg:?}");
                }
                let elapsed = self.ping_start.elapsed();
                //println!("MyWs::Pong elapsed = {:?}", elapsed);
                let ms = elapsed.as_millis() as usize;
                // Warn user when the delay becomes half the Tick
                ws_send_to_client(ClientCommand::PingTime { ms }, ctx);
            }
            Ok(ws::Message::Continuation(item)) => {
                use actix_http::ws::Item;
                debug!("> Continuation(item) ...",);
                match item {
                    Item::FirstText(bytes) | Item::FirstBinary(bytes) => {
                        debug!(">> Clearing buffer");
                        self.upload_buffer.clear();
                        self.upload_buffer.extend_from_slice(&bytes);
                    }
                    Item::Continue(bytes) => {
                        self.upload_buffer.extend_from_slice(&bytes.as_ref());
                    }
                    Item::Last(bytes) => {
                        self.upload_buffer.extend_from_slice(&bytes.as_ref());
                        debug!("> binary upload complete");
                        self.handle_upload(ctx);
                    }
                }
            }
            Ok(ws::Message::Binary(bin)) => {
                debug!("> received binary data {}", bin.len());

                self.upload_buffer = bin.to_vec();
                self.handle_upload(ctx);
                //ctx.binary(bin)
            }
            Ok(ws::Message::Text(text)) => {
                let msg: Result<ClientRequest, _> = serde_json::from_str(&text);
                let msg = match msg {
                    Ok(msg) => msg,
                    Err(e) => {
                        let msg = format!("{e:?} <- {text}");
                        info!("E> {msg}");
                        let msg = ClientCommand::InvalidInput(msg);
                        ws_send_to_client(msg, ctx);
                        return;
                    }
                };

                debug!("> ClientRequest {msg:?}");
                match msg {
                    ClientRequest::Hello => { /* OK */ }
                    ClientRequest::UploadAnnouncement(file_name) => {
                        self.next_file_name = Some(file_name);
                    }
                }
            }
            Err(err) => {
                let msg = format!(
                    "MyWs::StreamHandler error > {err:?} < upload_buffer: {} Bytes",
                    self.upload_buffer.len()
                );
                self.garbage_collect_buffer();
                warn!("{msg}");
                ws_send_to_client(ClientCommand::InvalidInput(msg), ctx);
            }
            _ => (),
        }
    }
}

/*
impl Handler<ClientCommand> for MyWs {
    type Result = ();

    fn handle(&mut self, msg: ClientCommand, ctx: &mut Self::Context) -> Self::Result {
        let msg = serde_json::to_string(&msg).unwrap();
        ctx.text(msg);
    }
}
*/

pub struct Lobby {
    active_websockets: Vec<Addr<MyWs>>,
}
impl Lobby {
    fn new() -> Addr<Self> {
        Self {
            active_websockets: Vec::with_capacity(1),
        }
        .start()
    }
}

impl Actor for Lobby {
    type Context = actix::Context<Self>;
}
impl Handler<SubscribeMyWs> for Lobby {
    type Result = ();

    fn handle(&mut self, msg: SubscribeMyWs, ctx: &mut Self::Context) -> Self::Result {
        self.active_websockets.push(msg.0);
        debug!("  Lobby has {} clients", self.active_websockets.len());
    }
}
impl Handler<UnsubscribeMyWs> for Lobby {
    type Result = ();

    fn handle(&mut self, msg: UnsubscribeMyWs, ctx: &mut Self::Context) -> Self::Result {
        self.active_websockets.retain(|e| *e != msg.0);
        debug!("  Lobby has {} clients", self.active_websockets.len());
    }
}

fn ws_send_to_client(msg: ClientCommand, ctx: &mut ws::WebsocketContext<MyWs>) {
    ctx.text(serde_json::to_string(&msg).expect("unable to serialize ClientCommand"))
}

fn handle_zipped_upload(
    upload_buffer: Vec<u8>,
    logger: &mut common::Logger,
    ctx: &mut ws::WebsocketContext<MyWs>,
) -> common::Result<Vec<u8>> {
    let InMemoryFile { name, contents } = unpack_zip_in_memory(upload_buffer)?;
    ws_send_to_client(ClientCommand::BinaryReceived(name.clone()), ctx);

    let parsed = common::parse(contents)?;
    let _ = common::collect_building_stats(&parsed, logger)?;
    let cleaned = common::clean_save(parsed, logger)?;

    let compressed = pack_zip_in_memory(name, cleaned)?;
    Ok(compressed)
}

fn unpack_zip_in_memory(buffer: Vec<u8>) -> std::io::Result<InMemoryFile> {
    use memfile::{CreateOptions, MemFile, Seal};
    use std::io::{Error, ErrorKind, Read, Write};

    let mut mem_file = MemFile::create("compressed.zip", CreateOptions::new().allow_sealing(true))?;
    mem_file.write_all(&buffer)?;
    drop(buffer);
    mem_file.add_seals(Seal::Write | /*Seal::Shrink |*/ Seal::Grow)?;

    let mut zip = zip::ZipArchive::new(mem_file)?;

    if zip.len() != 1 {
        return Err(Error::new(ErrorKind::InvalidData, "Invalid ZIP data!"));
    }
    let mut file = zip.by_index(0)?;
    let name = file.name().to_owned();
    debug!("Filename: {}", name);
    let mut contents = String::with_capacity(file.size() as usize);
    file.read_to_string(&mut contents);
    drop(file);

    let mut mem_file = zip.into_inner();
    mem_file.set_len(0);
    //libc::close(mem_file.as_raw_fd());

    Ok(InMemoryFile { name, contents })
}

pub struct InMemoryFile {
    name: String,
    contents: String,
}

fn pack_zip_in_memory(name: String, contents: String) -> std::io::Result<Vec<u8>> {
    use memfile::{CreateOptions, MemFile, Seal};
    use std::io::{Error, ErrorKind, Read, Seek, Write};

    let mem_file = MemFile::create_sealable("compressing.zip")?;
    let mut zip = zip::ZipWriter::new(mem_file);
    zip.start_file(
        name,
        zip::write::FileOptions::default()
            .compression_method(zip::CompressionMethod::Deflated)
            .compression_level(Some(9)),
    )?;

    zip.write_all(contents.as_bytes())?;

    let mut mem_file: MemFile = zip.finish()?;
    mem_file.add_seals(Seal::Write | /*Seal::Shrink |*/ Seal::Grow)?;

    mem_file.rewind()?;
    let mut buffer = vec![];
    mem_file.read_to_end(&mut buffer)?;

    mem_file.set_len(0);

    Ok(buffer)
}
