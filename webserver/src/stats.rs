use prometheus::{register_int_counter_vec, IntCounterVec};

pub struct Stats {
    websocket: IntCounterVec,
    pages: IntCounterVec,
    processing: IntCounterVec,
}

impl Stats {
    pub fn new() -> Self {
        let s = Stats {
            websocket: register_int_counter_vec!(
                "infraspace_trainstation_queue_websocket_count",
                "websocket success or error counts.",
                &["kind"]
            )
            .unwrap(),
            pages: register_int_counter_vec!(
                "infraspace_trainstation_queue_pages_count",
                "Access to specific pages.",
                &["page"]
            )
            .unwrap(),
            processing: register_int_counter_vec!(
                "infraspace_trainstation_processing_count",
                "Number of Saves processed.",
                &["status"]
            )
            .unwrap(),
        };

        s.websocket.with_label_values(&["ok"]).reset();
        s.websocket.with_label_values(&["err"]).reset();
        s.pages.with_label_values(&["index"]).reset();
        s.pages.with_label_values(&["metrics"]).reset();
        s.processing.with_label_values(&["ok"]).reset();
        s.processing.with_label_values(&["err"]).reset();

        s
    }

    pub fn index_inc(&self) {
        self.pages.with_label_values(&["index"]).inc();
    }
    pub fn metrics_inc(&self) {
        self.pages.with_label_values(&["metrics"]).inc();
    }
    pub fn ws_ok_inc(&self) {
        self.websocket.with_label_values(&["ok"]).inc();
    }
    pub fn ws_err_inc(&self) {
        self.websocket.with_label_values(&["err"]).inc();
    }
    pub fn processing_ok_inc(&self) {
        self.processing.with_label_values(&["ok"]).inc();
    }
    pub fn processing_err_inc(&self) {
        self.processing.with_label_values(&["err"]).inc();
    }
}
