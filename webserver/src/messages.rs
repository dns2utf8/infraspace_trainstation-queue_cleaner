use actix::{Actor, Addr, AsyncContext, Handler, Message, StreamHandler};
use actix_web::{web, App, Error, HttpRequest, HttpResponse, HttpServer, Responder};
use actix_web_actors::ws;
use serde::{Deserialize, Serialize};

use crate::MyWs;

#[derive(Debug, Deserialize, Serialize, Message)]
#[rtype(result = "()")]
pub enum ClientRequest {
    Hello,
    UploadAnnouncement(String),
}

#[derive(Debug, Deserialize, Serialize, Message)]
#[rtype(result = "()")]
pub enum ClientCommand {
    InvalidInput(String),
    BinaryReceived(String),
    BinaryProcessingLog { name: Option<String>, log: String },
    PingTime { ms: usize },
}

pub fn debug_client_commands() {
    fn t<S: Serialize>(msg: S) {
        println!("  {}", serde_json::to_string(&msg).unwrap());
    };

    println!("\n:: ClientRequest ::");
    t(ClientRequest::Hello);
    t(ClientRequest::UploadAnnouncement(format!("filename.sav")));

    println!("\n:: ClientCommand ::");
    t(ClientCommand::InvalidInput(format!("bla bla")));
    t(ClientCommand::BinaryReceived(format!("name.sav")));
    t(ClientCommand::BinaryProcessingLog {
        name: None,
        log: format!("no name"),
    });
    t(ClientCommand::BinaryProcessingLog {
        name: Some(format!("just.sav")),
        log: format!("just name"),
    });
    t(ClientCommand::PingTime { ms: 42 });

    println!("\n");
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct SubscribeMyWs(pub Addr<MyWs>);
#[derive(Message, Clone)]
#[rtype(result = "()")]
pub struct UnsubscribeMyWs(pub Addr<MyWs>);
