use actix_web::HttpResponse;
use log::{log, warn};
use serde::Serialize;
use tera::Tera;

const PAGECLASS_KEY: &'static str = "pageclass";

pub fn new() -> actix_web::web::Data<Templates> {
    actix_web::web::Data::new(Templates {
        tera: Tera::new("templates/**/*.html").expect("unable to load tera templates"),
    })
}

pub struct Templates {
    tera: Tera,
}

impl Templates {
    pub fn get_render_context<'a>(&'a self, user: &'a crate::UserContext) -> RenderContext<'a> {
        // default context
        let mut ctx = tera::Context::new();
        ctx.insert(
            "header_links",
            &[
                link("/", "Reset Fixing Tool"),
                link("https://estada.ch/support-my-work/", "Support this tool"),
                link("https://gitlab.com/dns2utf8/infraspace_trainstation-queue_cleaner", "Source Code of this tool"),
                link("https://forum.dionicsoftware.com/t/train-station-bug-causes-tousands-of-cars-to-be-keept-arond-crashing-economy-with-workaround/2837", "Forum discussing the issue"),
            ],
        );

        RenderContext {
            tera: &self.tera,
            ctx,
            user,
        }
    }
}

pub struct RenderContext<'a> {
    tera: &'a Tera,
    user: &'a crate::UserContext,
    ctx: tera::Context,
}

impl<'a> RenderContext<'a> {
    pub fn insert(&mut self, key: &str, value: &str) {
        self.ctx.insert(key, value)
    }

    /// render a page like `index.html`
    pub fn render_page(&mut self, name: &str) -> Result<HttpResponse, actix_web::Error> {
        if let Some(name) = name.rsplitn(1, ".html").next() {
            self.ctx.insert(PAGECLASS_KEY, name);
        }
        let s = self.tera.render(name, &self.ctx).map_err(|e| {
            warn!("index: {:?}", e);
            actix_web::error::ErrorInternalServerError(format!(
                "Template error in \"{name}\": {e:?}"
            ))
        })?;

        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }
}

#[derive(Debug, Serialize)]
struct Link {
    url: String,
    name: String,
}

fn link<S: AsRef<str>>(url: S, name: S) -> Link {
    Link {
        url: url.as_ref().to_owned(),
        name: name.as_ref().to_owned(),
    }
}
