"use strict";

window.onerror = function (msg, url, line) {
    alert("Message : " + msg
        + "\nURL : " + url
        + "\nLine number : " + line
        + "\nWS URL: " + ((location.protocol === 'http:' ? 'ws://' : 'wss://') + location.host + '/ws/') );
}

const form = document.querySelector("form");
form.onsubmit = handle_form_upload;
const file_upload = form.querySelector("input[type=file]");
file_upload.onchange = handle_file_upload;

let ws = undefined;

connect_websocket();

function connect_websocket() {
    ws = new WebSocket((location.protocol === 'http:' ? 'ws://' : 'wss://') + location.host + '/ws/');

    setTimeout(function() {
        if (ws.readyState !== WebSocket.OPEN) {
            status_text('Unable to connect within 5 seconds! Please try to reload the page and check the system requirements');
        }
    }, 5*1000);


    ws.onopen = function (event) {
        send('Hello');
        document.querySelector("form").classList.remove("hidden");
        status_text('Connected -> select files for processing');
        status_text(`WebSocket.binaryType = ${ws.binaryType}`, true);
    };


    ws.onmessage = async function (event) {
        try {
            if (typeof event.data === 'string') {
                const command = JSON.parse(event.data);
                for (const cmd in command) {
                    const msg = command[cmd];
                    console.log(['cmd', cmd, msg]);
                    switch (cmd) {
                        // View Commands
                        case "InvalidInput":
                            status_text(msg);
                            break;
                            
                        case "BinaryReceived":
                            const file = files_to_process.find(f => f.file.name == msg);
                            file.status = "Processed";
                            file.next_action = "Save cleaned .sav";
                            status_text(`Downloading cleaned file ${msg}`);
                            break;
                            
                        case "BinaryProcessingLog":
                            if (typeof msg.name === 'string') {
                                const file = files_to_process.find(f => f.file.name == msg.name);
                                if (file) {
                                    file.log = msg.log;
                                }
                            }
                            status_text(`::: LOG: ${msg.name} :::\n${msg.log}`, true)
                            
                            break;
                            
                        case "PingTime":
                            status_text(`PingTime: ${msg.ms}ms`, true);
                            break;

                        default:
                            console.error(["unimplemented command", cmd]);
                            break;
                    }
                }
            } else {
                console.log(["zip", event.data]);
                // decode downloaded ZIP
                let zip = new JSZip();
                await zip.loadAsync(event.data);
                
                const filename = Object.keys(zip.files)[0];
                //const filename = zip.files[0];
                const uncompressed = await zip.file(filename).async("string");
                
                
                console.log(["uncompressed", uncompressed]);
                
                const file = files_to_process.find(f => f.file.name == filename);
                file.cleaned = uncompressed;
                file.status = "Ready";
                file.next_action = "Save Cleaned File";
                file.submittable = true;
                file.disabled = false;
                
                update_table_view();
            }
        } catch (error) {
            console.error(["invalid command from server", error, event.data]);
            throw error;
        }
    };

    ws.onclose = function (event) {
        console.log('Lost connection');
        status_text('Lost connection', true);
        
        files_to_process.forEach(file => {
            if (file.status.startsWith("Uploading") === true) {
                file.status = "Loaded";
                file.next_action = "Upload & Process";
                file.disabled = false;
            }
        });
        update_table_view();

        check_online();
        async function check_online() {
            try { 
                const a = await fetch('/ws/');
                if (a.status === 400) {
                    console.log("reloading... /ws/ === 400");
                    if (document.location.hostname === 'localhost') {
                        location.reload(true);
                    } else {
                        connect_websocket();
                    }
                } else {
                    requestAnimationFrame(_ => {
                        setTimeout(check_online, 1000);
                    });
                }
            } catch (error) {
                requestAnimationFrame(_ => {
                    setTimeout(check_online, 1000);
                });
            }
        }
    };
}

function send(msg) {
    ws.send(JSON.stringify(msg));
}

function status_text(text, append) {
    const element = document.querySelector("#status_text");
    if (append) {
        element.innerText += `${text}\n`;
    } else {
        element.innerText = `${text}\n`;
    }
}

const table = document.querySelector("tbody");
const files_to_process = [];
async function handle_file_upload(ev) {
    if (ev && ev.preventDefault) {
        ev.preventDefault();
    }
    
    for (let i = 0; i < file_upload.files.length; ++i) {
        const file = file_upload.files[i];
        files_to_process.push(new FileInfo(file));
        console.log(["file", file.name]);
    }
    
    update_table_view();
    
    for (let file of files_to_process) {
        file.data = await file.data;
        file.disabled = false;
        if (file.zip_data === undefined) {
            file.status = "Loaded";
            file.next_action = "Upload & Process";
        }
    }
    update_table_view();
}

function update_table_view() {
    let html = '';
    for (const file of files_to_process) {
        html += `<tr>
            <td>${file.id}</td>
            <td>${file.file.name}</td>
            <td>${file.readable_size}</td>
            <td>${file.status}</td>
            <td><button data-id="${JSON.stringify(file.id)}" ${file.submittable ? 'type="submit"' : ''} ${file.disabled ? 'disabled=""' : ''}>${file.next_action}</button></td>
        </tr>`;
    }
    table.innerHTML = html;
    table.querySelectorAll("input").onclick = handle_button_upload;
}

let global_id = 0;
function FileInfo(file) {
    this.id = global_id++;
    this.file = file;
    this.readable_size = format_size(file.size);
    this.last_modified = new Date(file.lastModified);
    
    this.data = file.arrayBuffer();
    this.zip_data = undefined;
    
    this.submittable = false;
    this.disabled = true;
    this.status = "Queued";
    this.next_action = "...";
}

function format_size(size) {
    if (typeof size !== 'number') {
        return "NaN";
    }
    const suffixes = ["Bytes", "KiB", "MiB", "GiB"];
    let i = 0;
    while (size > 1024 && i < suffixes.length - 1) {
        size /= 1024;
        i += 1;
    }
    return `${size.toFixed(i === 0 ? 0 : 2)} ${suffixes[i]}`;
}

async function handle_form_upload(ev) {
    if (ev && ev.preventDefault) {
        ev.preventDefault();
    }
    
    const id = JSON.parse( ev.submitter.dataset["id"] );
    const file = files_to_process.find(f => f.id == id);
    
    if (file.zip_data === undefined) {
        const zip = new JSZip();
        file.next_action = "...";
        file.status = "Compressing ...";
        file.disabled = true;
        console.log(file.status);
        update_table_view();
        zip.file(file.file.name, file.data);
        //file.zip_data = await zip.generateAsync({type: "uint8array"});
        file.zip_data = await zip.generateAsync({
            type: "ArrayBuffer",
            compression: "DEFLATE",
            compressionOptions: {
                level: 9
            }
        });
    
        const zip_len = file.zip_data.length || file.zip_data.byteLength;
        file.status = `Uploading ${format_size(zip_len)}...`;
        console.log(file.status);
        update_table_view();
        send({ "UploadAnnouncement": file.file.name });
        ws.send(file.zip_data);
    }
    
    if (file.status === "Ready") {
        const blob = new Blob([file.cleaned], {type: "text/plain"});
        const blobUrl = URL.createObjectURL(blob, {type: "text/plain"});
    
        const a = document.createElement("a");
        a.href = blobUrl;
        a.download = file.file.name.split(".sav", -1)[0] + ".cleaned.sav";
        a.click();
    }
    
    // stop the form from submitting
    return false;
}

function handle_button_upload(ev) {
    if (ev && ev.preventDefault) {
        ev.preventDefault();
    }
    
    debugger;
    
    // stop the form from submitting
    return false;
}
