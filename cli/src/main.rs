use std::{
    fs::{self, read_to_string, write},
    io,
    path::{Path, PathBuf},
};

#[cfg(target_os = "windows")]
fn plattform_candidates() -> Vec<PathBuf> {
    let candidates = ["%AppData%\\..\\LocalLow\\Dionic Software\\InfraSpace\\saves\\"];
    let app_data = dirs::data_local_dir().expect("unable to find %AppData%");
    todo!()
}

#[cfg(target_os = "linux")]
fn plattform_candidates() -> Vec<PathBuf> {
    let home = dirs::home_dir().expect("unable to find users home_dir");
    let candidates=[
        ("flatpack steam", ".var/app/com.valvesoftware.Steam/.local/share/Steam/steamapps/compatdata/1511460/pfx/drive_c/users/steamuser/AppData/LocalLow/Dionic Software/InfraSpace/saves/"),
        ("linux steam", ".steam/steam/SteamApps/compatdata/1511460/pfx/drive_c/users/steamuser/Saved Games/Dionic Software/InfraSpace/saves/"),
    ];
    let mut paths = vec![];
    for (name, path) in candidates {
        let full_path = home.join(path);
        if full_path.is_dir() {
            println!("found {name}: {full_path:?}");
            paths.push(full_path);
        } else {
            println!("NO {name}: {full_path:?}");
        }
    }
    paths
}

fn main() -> common::Result<()> {
    let candidates = plattform_candidates();

    // List saves
    let mut versions: Vec<PathBuf> = vec![];
    for c in candidates {
        let mut lc = list_dir(c, true)?;
        versions.append(&mut lc);
    }
    // Alpha 9.1.204/
    let version = user_choose_dir(&versions, "Choose Version", versions.len() - 1)?;
    let saves = list_dir(version, false)?;
    let save = user_choose_dir(&saves, "Choose Save File", saves.len() - 1)?;

    println!("\nCleaning {:?} ...", save.display());
    let mut new_name = save.with_extension("cleaned.sav");
    println!("\nPreparing new file {:?} ...", new_name.display());

    let mut logger = common::Logger::default();

    let original = read_to_string(save)?;
    let parsed = common::parse(original)?;

    loop {
        match read_stdin_string("Action: 'clean' or 'map' or 'delcars'")?.trim() {
            "clean" | "c" => {
                let cleaned = common::clean_save(parsed, &mut logger)
                    .map_err(|e| {
                        eprintln!("{}", logger.finish());
                        e
                    })
                    .expect("json processing error");
                println!("{}", logger.finish());

                while new_name.exists() {
                    let q = format!(
                        "a save with the name \"{}\" already exists. Over write?",
                        new_name.display()
                    );
                    if read_stdin_bool(&q, true)? {
                        break;
                    } else {
                        new_name.set_file_name(read_stdin_string("Please enter a new name")?);
                    }
                }

                write(&new_name, cleaned)?;
                break;
            }
            "map" | "m" => {
                let stats = common::collect_building_stats(&parsed, &mut logger);
                todo!("mapping: {stats:#?}")
            }
            "delcars" | "d" => {
                todo!("delcars")
            }
            _ => {
                //true,
                println!("invalid selection, try again");
            }
        }
    }
    println!("Success!");
    Ok(())
}

fn list_dir<P: AsRef<Path>>(path: P, is_dir: bool) -> io::Result<Vec<PathBuf>> {
    let mut entries = vec![];
    for e in fs::read_dir(path)? {
        let p = e?.path();
        if p.is_dir() == is_dir {
            entries.push(p);
        }
    }

    Ok(entries)
}

fn user_choose_dir(choices: &Vec<PathBuf>, msg: &str, default: usize) -> io::Result<PathBuf> {
    println!("\n:: {msg} ::\n");
    let len = choices.len();
    if len == 0 {
        panic!("no choices");
    }
    for (i, p) in choices.iter().enumerate() {
        let def = if i == default { "*" } else { "" };
        println!("[{def}{i}] {:?}", p.file_name());
    }
    let choice = loop {
        let input = read_stdin(default)?;
        if input < len {
            break input;
        }
    };

    Ok(choices[choice].clone())
}

fn read_stdin(default: usize) -> io::Result<usize> {
    loop {
        print!("\npick one (default: {}): ", default);
        flush_stdout()?;

        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer)?;
        let snip = buffer.trim();

        if snip == "q" {
            panic!("aborting");
        }
        if snip == "" {
            return Ok(default);
        }

        match snip.parse() {
            Ok(i) => return Ok(i),
            Err(e) => println!("{:?}", e),
        }
    }
}

fn read_stdin_bool(text: &str, default: bool) -> io::Result<bool> {
    loop {
        print!("\n{text} (y/n; default: {}): ", default);
        flush_stdout()?;

        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer)?;

        let buffer = buffer.to_lowercase();
        let snip = buffer.trim();

        match snip {
            "" => return Ok(default),
            "y" | "t" | "true" => return Ok(true),
            "n" | "f" | "false" => return Ok(false),
            "q" => panic!("aborting"),
            invalid => println!("invalid input {invalid:?}"),
        }
    }
}

fn read_stdin_string(text: &str) -> io::Result<String> {
    loop {
        print!("\n{text}: ");
        flush_stdout()?;

        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer)?;

        let snip = buffer.trim();

        match snip {
            "" => println!("you have to enter something"),
            something => return Ok(something.to_owned()),
        }
    }
}

fn flush_stdout() -> std::io::Result<()> {
    use std::io::Write;
    std::io::stdout().flush()?;
    std::io::stderr().flush()
}
