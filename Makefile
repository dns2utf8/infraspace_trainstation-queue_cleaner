DOMAIN = "infraspace.estada.at"
HOST = "web_${DOMAIN}@lich.estada.ch"
BINARY = "webserver"
SERVICE_FILE = "itqc.service"
CONTAINER ?= docker

deploy: build_release deploy_direct
	# done

deploy_direct_dryrun:
	rsync -avz --delete-after --dry-run ./target/release/${BINARY} ./webserver/static ./webserver/templates ${HOST}:
	make systemd_restart

deploy_direct:
	ssh ${HOST} -t "systemctl --user stop ${SERVICE_FILE} || true"
	rsync -avz --delete-after ./target/release/${BINARY} ./webserver/static ./webserver/templates ${HOST}:
	make systemd_restart

systemd_setup:
	ssh ${HOST} "mkdir -p ~/.config/systemd/user/"
	scp ${SERVICE_FILE} ${HOST}:~/.config/systemd/user/${SERVICE_FILE}
	ssh ${HOST} -t "systemctl --user daemon-reload ; systemctl --user restart ${SERVICE_FILE} ; systemctl --user enable ${SERVICE_FILE} ; systemctl --user status ${SERVICE_FILE}"

systemd_stop:
	ssh ${HOST} -t "systemctl --user stop ${SERVICE_FILE} ; systemctl --user status ${SERVICE_FILE}"

systemd_restart:
	ssh ${HOST} -t "systemctl --user restart ${SERVICE_FILE} ; systemctl --user status ${SERVICE_FILE}"

journal:
	ssh ${HOST} -t "journalctl --user --unit ${SERVICE_FILE} -efx"


build_release:
	cd webserver && cargo build --release

build_in_container_cached:
	${CONTAINER} run --rm -it --user "$$(id -u)":"$$(id -g)" -v "$$PWD":/usr/src/myapp -v "$$HOME"./cargo/registry/:/usr/local/cargo/registry/:ro -w /usr/src/myapp rust:latest cargo build --release --bin ${BINARY}
	chown "$$(id -u)":"$$(id -g)" target/release/${BINARY}

build_in_container:
	${CONTAINER} run --rm --user "$$(id -u)":"$$(id -g)" -v "$$PWD":/usr/src/myapp -w /usr/src/myapp rust:latest cargo build --release --bin ${BINARY}
	chown "$$(id -u)":"$$(id -g)" target/release/${BINARY}

ssh:
	ssh ${HOST}

ws_debug:
	websocat ws://localhost:8080/ws/
