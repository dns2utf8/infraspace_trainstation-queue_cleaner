use serde_json::{Map, Value};
use std::collections::HashMap;

pub type Result<T> = std::result::Result<T, Error>;

pub fn parse(original: String) -> Result<ParsedFile> {
    let lines: std::str::SplitN<_> = original.splitn(4, "\n");

    // Clone the iterator not the entire file
    let header = lines.clone().take(3).collect::<Vec<_>>().join("\n");
    let data: Value = serde_json::from_str(
        lines
            .skip(3)
            .next()
            .ok_or(access_err("missing fourth line in save file"))?,
    )?;

    Ok(ParsedFile { header, data })
}

#[derive(Debug, PartialEq)]
pub struct ParsedFile {
    header: String,
    /// The JSON data
    data: Value,
}
impl ParsedFile {
    pub fn serialize(self) -> Result<String> {
        Ok(format!(
            "{}\n{}",
            self.header,
            serde_json::to_string_pretty(&self.data)?
        ))
    }
}

#[derive(Default)]
pub struct BuildingStats {
    counts: HashMap<String, usize>,
}

impl core::fmt::Debug for BuildingStats {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("BuildingStats ")?;

        let mut total_amount = 0;
        let mut list: Vec<_> = self
            .counts
            .iter()
            .map(|e| {
                total_amount += e.1;
                e
            })
            .collect();
        list.sort();

        let mut dl = f.debug_list();
        let total_amount = total_amount as f64;
        for e in list {
            dl.entry(&format!(
                "{}: {} ({:.2}%)",
                e.0,
                e.1,
                100.0 * (*e.1 as f64) / total_amount
            ));
        }

        dl.finish()
    }
}

pub fn collect_building_stats(parsed: &ParsedFile, logger: &mut Logger) -> Result<BuildingStats> {
    logger.ln("\n  :: Collecting BuildingStats :: ");
    let mut stats: BuildingStats = Default::default();

    // .pointer_mut("/buildings")
    let buildings: &Vec<Value> = parsed
        .data
        .get("buildings")
        .and_then(|f| f.as_array())
        .ok_or(access_err("unable to find /buildings"))?;
    for e in buildings {
        let obj = e
            .as_object()
            .ok_or(access_err("building is not an object"))?;
        // the type of building
        let buildingName = obj
            .get("buildingName")
            .ok_or(access_err("unable to find /buildings[x].buildingName"))?
            .as_str()
            //.as_ref()
            .ok_or(access_err("/buildings[x].buildingName is not a String"))?
            .to_string();

        *stats.counts.entry(buildingName).or_default() += 1;
    }

    logger.ln(format!("{stats:#?}"));
    Ok(stats)
}

pub fn fix_resource_amounts(parsed: &mut ParsedFile, logger: &mut Logger) -> Result<()> {
    logger.ln("\n\nfix_resource_amounts ...");
    // <String type, (at mines, in cars, factory inputs)>
    let mut available_resources: HashMap<String, (u64, u64, u64)> = HashMap::new();

    // find the cars
    let cars: &mut Vec<Value> = parsed
        .data
        .get_mut("cars")
        .and_then(|f| f.as_array_mut())
        .ok_or(access_err("unable to find /cars"))?;
    for e in cars {
        let obj = e.as_object().ok_or(access_err("car is not an object"))?;
        let amount: u64 = obj
            .get("amount")
            .and_then(|a: &Value| a.as_u64())
            .ok_or(access_err("/cars[x].amount as not a number"))?;
        if amount > 0 {
            let resource = obj
                .get("resource")
                .and_then(|r| r.as_str())
                .ok_or(access_err("/cars[x].resource is not a string"))?
                .to_string();

            available_resources.entry(resource).or_default().1 += amount;
        }
    }

    let buildings: &mut Vec<Value> = parsed
        .data
        .get_mut("buildings")
        .and_then(|f| f.as_array_mut())
        .ok_or(access_err("unable to find /buildings"))?;

    logger.ln("finding the factories and mines ...");
    for obj in buildings {
        /*let obj:() = obj
        .as_object_mut()
        .ok_or(access_err("building is not an object"))?;*/
        {
            // the type of building
            let buildingName: &str =
                obj.get("buildingName")
                    .and_then(|b| b.as_str())
                    .ok_or(access_err(
                        "unable to find /buildings[x].buildingName as str",
                    ))?;

            logger.ln(format!(
                "  > buildingName: {:?} <- {}",
                buildingName,
                buildingName.ends_with("Mine")
            ));
            // find the mines
            if buildingName.ends_with("Mine") {
                let resource = buildingName
                    .trim_start_matches("large")
                    .trim_end_matches("Mine")
                    .to_lowercase();
                logger.ln(format!("  - resource: {resource}"));
                let outgoingStorage = obj
                    .pointer("/consumerProducer/outgoingStorage/0")
                    .and_then(|u| u.as_u64())
                    .ok_or(access_err(
                        "/buildings[x].consumerProducer.outgoingStorage[0] is not a u64",
                    ))?;
                available_resources.entry(resource).or_default().0 += outgoingStorage;
            }
        }

        // find the factories like "buildingName": "vegetableFarm",
        let mut show = false;
        if let Some(consumerProducer) = obj.get_mut("consumerProducer") {
            if let Some(consumables) = consumerProducer
                .pointer_mut("/productionLogic/productionDefinition/consumables")
                .and_then(|a| a.as_array())
            {
                let n_inputs = consumables.len();
                let incomingStorage: &mut Vec<_> = consumerProducer
                    .get_mut("incomingStorage")
                    .and_then(|a| a.as_array_mut())
                    .ok_or(access_err("/buildings[x].incomingStorage is not an array"))?;
                for mut incom in incomingStorage.iter_mut().take(n_inputs) {
                    if let Some(n) = incom.as_u64() {
                        *incom = Value::Number(n.max(60).into());
                    }
                }

                let outgoingStorage: &mut Vec<_> = consumerProducer
                    .get_mut("outgoingStorage")
                    .and_then(|a| a.as_array_mut())
                    .ok_or(access_err("/buildings[x].outgoingStorage is not an array"))?;
                show = outgoingStorage.len() > 1;
            }
        }
        if show {
            logger.ln(format!("    FF: {:?}", obj));
        }
    }

    Ok(())
}

pub fn clean_save(mut parsed: ParsedFile, logger: &mut Logger) -> Result<String> {
    ////////////////////////////////////// find resources in train stations
    let mut cars_to_garbage_collect: Vec<u64> = vec![];

    // .pointer_mut("/buildings")
    let buildings: &mut Vec<Value> = parsed
        .data
        .get_mut("buildings")
        .and_then(|f| f.as_array_mut())
        .ok_or(access_err("unable to find /buildings"))?;
    for e in buildings {
        let obj = e
            .as_object_mut()
            .ok_or(access_err("building is not an object"))?;
        let name = format!(
            "{:?}",
            obj.get("customName")
                .ok_or(access_err("unable to find /buildings[x].customName"))?
                .as_str()
                .as_ref()
        );
        match &mut obj
            .get_mut("stationModule")
            .ok_or(access_err("unable to find /buildings[x].stationModule"))?
        {
            Value::Null => { /* skip */ }
            Value::Object(kv_map) => {
                let mut n_queued = 0usize;
                let queued: &mut Vec<Value> = kv_map
                    .get_mut("carsWaitingForRide")
                    .and_then(|m| m.as_array_mut())
                    .ok_or(access_err(
                        "unable to find /buildings[x].stationModule.carsWaitingForRide",
                    ))?;
                for arr_elem in queued.iter() {
                    let obj: &Map<_, _> = arr_elem.as_object().ok_or(access_err(
                        "unable to get queued /buildings[x].stationModule.carsWaitingForRide.cars",
                    ))?;
                    for (_key, value) in obj {
                        n_queued += 1;
                        cars_to_garbage_collect.push(
                            value
                                .as_u64()
                                .ok_or(access_err("unable to get object id"))?,
                        );
                    }
                }
                logger.ln(format!(
                    "    {}: len({}) queued({})",
                    name,
                    queued.len(),
                    n_queued
                ));
                queued.clear();
            }
            _unexpected => {
                return Err(access_err("unexpected type in /buildings[x]"));
            }
        }
    }

    /////////////////////////////////// gc objects
    logger.ln("\nCollecting Garbage Cars...");
    cars_to_garbage_collect.sort();
    let mut gc_found = 0usize;
    let mut gc_not_found = 0usize;
    let mut errors = vec![];
    let cars = parsed
        .data
        .get_mut("cars")
        .and_then(|c| c.as_array_mut())
        .ok_or(access_err("unable to find /cars"))?;
    cars.retain(|e| {
        let obj = match e.as_object() {
            Some(o) => o,
            None => {
                errors.push(access_err("/cars/[element] is not an object"));
                return true;
            }
        };
        let id = match obj.get("ID").and_then(|i| i.as_u64()) {
            Some(o) => o,
            None => {
                errors.push(access_err("unable to find /cars/[element]/ID as u64"));
                return true;
            }
        };
        match cars_to_garbage_collect.binary_search(&id) {
            Ok(_) => {
                if gc_found == 0 {
                    logger.ln(format!("    First removing ID: {id}"));
                }
                gc_found += 1;
                false
            }
            Err(_) => {
                if gc_not_found == 0 {
                    logger.ln(format!("    First NOT removing ID: {id}"));
                }
                gc_not_found += 1;
                true
            }
        }
    });

    logger.ln(format!(
        "to GC: {}; gc_found: {gc_found}; gc_keep: {gc_not_found}",
        cars_to_garbage_collect.len()
    ));

    fix_resource_amounts(&mut parsed, logger)?;

    /////////////////////////////////// save
    parsed.serialize()
}

#[derive(Default)]
pub struct Logger {
    buffer: Vec<String>,
}
impl Logger {
    /// Log and append \n
    pub fn ln<S: Into<String>>(&mut self, line: S) {
        self.buffer.push(line.into());
    }
    pub fn finish(&self) -> String {
        self.buffer.join("\n")
    }
}

#[derive(Debug)]
pub enum Error {
    IO(std::io::Error),
    Parsing(serde_json::Error),
    Access(String),
}
impl From<serde_json::Error> for Error {
    fn from(base: serde_json::Error) -> Self {
        Error::Parsing(base)
    }
}
impl From<std::io::Error> for Error {
    fn from(base: std::io::Error) -> Self {
        Error::IO(base)
    }
}
impl PartialEq<Error> for Error {
    fn eq(&self, other: &Error) -> bool {
        match (self, other) {
            (Error::Parsing(s), Error::Parsing(o)) => format!("{s:?}") == format!("{o:?}"),
            (Error::Access(s), Error::Access(o)) => s == o,
            _ => false,
        }
    }
}
pub fn access_err<S: Into<String>>(msg: S) -> Error {
    Error::Access(msg.into())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn input_too_short() {
        let r = parse("bla".into());
        assert_eq!(Err(access_err("missing fourth line in save file")), r);
    }

    #[test]
    fn input_header_to_short() {
        let r = parse("bla\n{}".into());
        assert_eq!(Err(access_err("missing fourth line in save file")), r);
    }

    #[test]
    fn input_body_empty() {
        let r = clean_save(parse("\n\n\n{}".into()).unwrap(), &mut Logger::default());
        assert_eq!(Err(access_err("unable to find /buildings")), r);
    }

    #[test]
    fn input_body_buildings_empty() {
        let r = clean_save(
            parse("\n\n\n{\"buildings\":[]}".into()).unwrap(),
            &mut Logger::default(),
        );
        assert_eq!(Err(access_err("unable to find /cars")), r);
    }

    #[test]
    fn input_body_buildings_cars_empty() {
        let input = "\n\n\n{\n  \"buildings\": [],\n  \"cars\": []\n}".to_string();
        let r = clean_save(parse(input.clone()).unwrap(), &mut Logger::default());
        assert_eq!(Ok(input), r);
    }

    #[test]
    fn io_result_conversion() -> Result<()> {
        let ok: std::io::Result<()> = Ok(());
        let ok: crate::Result<()> = Ok(ok?);
        ok
    }
}
